# README #

Unix like tools to inspect the structure of data: PCA and heatmap.

### Installation ###

Anyone willing to build a conda package??

### PCA ###

	Usage: pca_plot [options] input_data input_info png_output pcs
                     .META: input_info
                        1. sample names
                        2... covariates
	Plotting PCA scoreplot. 
                     'input_data' is the path of a delimited file containing numeric data on which PCA is performed. Each column represents a different sample, each row corresponds to a gene. The header (sample names) must be defined.
                     'input_info' is the path of a delimited file containing informations about samples classification. The first column must contain sample names, the following ones represent covariates. The header (names of covariates) must be defined.
                     There have to be a perfect matching between sample names as reported in dataset (column names) and in the informations file (first column).
                     'png_output' is the path on where to save the produced scoreplot. It must be a .png file
                     'pcs' is the couple of principal components to represent. They have to be separated by a comma and no blank spaces (for example: 1,2 or 3,4 ecc).
                     Samples can be represented with different colors and/or shapes depending on the specified covariates. Not more than two covariates can be handled at the same time.

	Options:
        -c CATEGORICAL, --categorical=CATEGORICAL
                Names of the categorical variables to be taken into account, written exactly as they are reported in the information file. There can be zero, one or at maximum TWO categorical variables. If more than one, variable names have to be separated by a comma and NO blank spaces. [default "NA"]

        -q QUANTITATIVE, --quantitative=QUANTITATIVE
                Name of the continuous variable to be taken into account, written exactly as it is reported in the information file. There can be at maximum ONE continuous variable. [default "NA"]

        -z, --zero_center
                Whether the variables should be shifted to be zero centered. [default "FALSE"]

        -s, --scale
                Whether the variables should be scaled to have unit variance. [default "FALSE"]

        -l, --loadings
                Whether the loadings matrix should be printed to screen. [default "FALSE"]

        -m, --mute
                Whether to silence sample labels in the plot. [default "FALSE"]

        -h, --help
                Show this help message and exit


### Heatmap ###

	Usage: heatmap [options] input_data input_info output.png
	Plotting heatmaps. 
                     'input_data' is the path of a delimited file (.txt) containing numeric values to be plotted. Each column must represent a different sample, each row must correspond to a gene. Header is required.
                     'input_info' is the path of a delimited file (.txt) containing informations about samples classification. The first column must contain sample names, the following ones must represent covariates. header is required.
                     There should be a perfect matching between sample names in dataset (column names) and in informations file (first column).
                     'output' is the path on where to save the produced heatmap. It must be a .png file.

	Options:
        -C COVARIATE, --covariate=COVARIATE
                Which variables in the input_info table have to be used as classification factors. If more than one, variable names have to be separated by a comma and NO blank spaces.

        -c CLUSTER, --cluster=CLUSTER
                Whether to perform clustering (dendrogram) on rows (1), on columns (2) or both (12).

        -w WIDTH, --width=WIDTH
                Width of the heatmap.

        -e HEIGHT, --height=HEIGHT
                Height of the heatmap.

        -s, --scale_row
                Whether to scale the data in order to get the same sum on each row. If it is used together with the transpose option, scaling is performed on columns. [default "FALSE"] 

        -t, --transpose
                Whether to transpose the data in order to plot different samples on different rows insead of columns. [default "FALSE"] 

        -p, --pal_colors
                Use more distinct colors in annotation palette. [default "FALSE"] 

        -d DISTANCE, --distance=DISTANCE
                Distance to use to compute dendrogram and clustering, possible values are: correlation, euclidean, maximum, manhattan, canberra, binary, minkowski, spearman. [default "euclidean"]

        -m CLUSTERING_METHOD, --clustering_method=CLUSTERING_METHOD
                Method to use to perform clustering, possible values are: ward.D, ward.D2, single, complete, average (= UPGMA), mcquitty (= WPGMA), median (= WPGMC) or centroid (= UPGMC). [default "complete"]

        -h, --help
                Show this help message and exit


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* This repo is howned by Ivan Molineris and Giulia Scotti